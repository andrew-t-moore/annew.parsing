﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Parsing.Tests {
    [TestClass]
    public class UnitTest1 {

        [TestMethod]
        public void ParserShouldBeAtEndOfInputImmediatelyOnAnEmptyString() {
            const string input = "";
            var parser = new StringParser(input);
            parser.AtEndOfInput.Should().BeTrue();
        }

        [TestMethod]
        public void ParserMustNotBeAtEndOfInputImmediatelyOnNonEmptyString() {
            const string input = " ";
            var parser = new StringParser(input);
            parser.AtEndOfInput.Should().BeFalse();
        }

        [TestMethod]
        public void SkipWhileWillPlaceTheCursorAtEndOfInputIfNoMatchIsFound() {
            const string input = "    ";
            var parser = new StringParser(input);
            parser.SkipWhile(' ');
            parser.AtEndOfInput.Should().BeTrue();
        }

        [TestMethod]
        public void SkipWhileCanSkipASingleCharacter() {
            const string input = " a";
            var parser = new StringParser(input);
            parser.SkipWhile(' ');
            parser.AtEndOfInput.Should().BeFalse();
            parser.Current.Should().Be('a');
        }

        [TestMethod]
        public void SkipWhileCanSkipMultipleCharacters() {
            const string input = "  a";
            var parser = new StringParser(input);
            parser.SkipWhile(' ');
            parser.AtEndOfInput.Should().BeFalse();
            parser.Current.Should().Be('a');
        }

        [TestMethod]
        public void SkipWhileCanSkipArraysOfCharacters() {
            const string input = "abababc";
            var parser = new StringParser(input);
            parser.SkipWhile('a', 'b');
            parser.AtEndOfInput.Should().BeFalse();
            parser.Current.Should().Be('c');
        }

        [TestMethod]
        public void SkippingUntilACharThatIsNotInTheInputWillPlaceCursorAtEndOfInput() {
            const string input = "    ";
            var parser = new StringParser(input);
            parser.SkipUntil('!');
            parser.AtEndOfInput.Should().BeTrue();
        }

        [TestMethod]
        public void SkipUntilStopsOnAnyMatchedCharacter() {
            const string input = "abababc";
            var parser = new StringParser(input);
            parser.SkipUntil('c', 'd', 'e');
            parser.AtEndOfInput.Should().BeFalse();
            parser.Current.Should().Be('c');
        }

        [TestMethod]
        public void TakingACharacterAdvancesTheCursorByOneCharacter() {
            const string input = "abc";
            var parser = new StringParser(input);

            var initialPosition = parser.Position;
            parser.Take();
            var newPosition = parser.Position;

            newPosition.Should().Be(initialPosition + 1);
        }

        [TestMethod]
        public void TakingACharacterReturnsTheCurrentCharacter() {
            const string input = "abc";
            var parser = new StringParser(input);

            var current = parser.Current;
            var taken = parser.Take();

            taken.Should().Be(current);
        }

        [TestMethod]
        public void TakeWhileCanReturnNoCharacters() {
            const string input = "   ";
            var parser = new StringParser(input);
            var characters = parser.TakeWhile('a');
            characters.Should().Be("");
        }

        [TestMethod]
        public void TakeWhileCanTakeAllCharactersUpToEndOfInput() {
            const string input = "abcd";
            var parser = new StringParser(input);
            var characters = parser.TakeWhile('a', 'b', 'c', 'd');
            characters.Should().Be("abcd");
            parser.AtEndOfInput.Should().BeTrue();
        }

        [TestMethod]
        public void SkipUntilThrowsExceptionIfCharsIsNull() {
            const string input = "abcd";
            var parser = new StringParser(input);

            Action action = () => parser.SkipUntil(null);

            action.ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void PeekShouldReturnTheNextCharacter() {
            const string input = "abcd";
            var parser = new StringParser(input);
            parser.Peek().Should().Be('b');
        }

        [TestMethod]
        public void PeekWithArgsShouldReturnTheNextCharacters() {
            const string input = "abcd";
            var parser = new StringParser(input);
            parser.Peek(2).Should().Be("bc");
        }
    }
}