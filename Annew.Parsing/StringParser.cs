﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Annew.Parsing {

    public class StringParser {
        private readonly string _input;
        private int _position;
        private readonly int _length;

        public StringParser(string input) {
            _input = input;
            _position = 0;
            _length = input.Length;
        }

        /// <summary>
        /// True if the parser has reached the end of the input, else false.
        /// </summary>
        public bool AtEndOfInput {
            get {
                return _position >= _length;
            }
        }

        /// <summary>
        /// The original input string.
        /// </summary>
        public string Input {
            get {
                return _input;
            }
        }

        /// <summary>
        /// The current 0-based position in the input.
        /// </summary>
        public int Position {
            get {
                return _position;
            }
        }

        /// <summary>
        /// The character at the current position in the input.
        /// </summary>
        public char Current {
            get {
                return _input[_position];
            }
        }

        /// <summary>
        /// Returns the next character in the input, without advancing the cursor.
        /// </summary>
        /// <returns></returns>
        public char Peek() {
            return _input[_position + 1];
        }

        /// <summary>
        /// Returns the next @chars characters in the input, without advancing the cursor.
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        public string Peek(int chars) {
            return _input.Substring(_position+1, chars);
        }

        /// <summary>
        /// Skips over the specified number of characters in the input.
        /// </summary>
        /// <param name="chars">The number of characters to skip.</param>
        public void Skip(int chars = 1) {
            if (chars < 1)
                throw new ArgumentOutOfRangeException("chars", "chars must be greater than 0");

            _position += chars;
        }

        /// <summary>
        /// Skips over characters while they match any of the supplied characters. After calling, 
        /// the parser will be positioned on the first character that doesn't match the supplied
        /// characters, or at end of input.
        /// </summary>
        /// <param name="chars">Characters to skip</param>
        public void SkipWhile(params char[] chars) {
            if (chars == null)
                throw new ArgumentNullException("chars");

            while (!AtEndOfInput) {
                if (chars.Contains(Current))
                    _position++;
                else {
                    return;
                }
            }
        }

        /// <summary>
        /// Skips over characters in the input until a character is found that matches any of
        /// the supplied characters. The parser will be positioned on the first character that
        /// doesn't match the supplied characters, or at end of input.
        /// </summary>
        /// <param name="chars"></param>
        public void SkipUntil(params char[] chars) {
            if (chars == null)
                throw new ArgumentNullException("chars");

            while (!AtEndOfInput) {
                if (chars.Contains(Current))
                    return;

                _position++;
            }
        }
         
        /// <summary>
        /// Takes a single character from the input, and advances the parser.
        /// </summary>
        /// <returns></returns>
        public char Take() {
            return _input[_position++];
        }

        /// <summary>
        /// Takes multiple characters from the input, and advances the parser.
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        public string Take(int chars) {
            if (chars < 1)
                throw new ArgumentOutOfRangeException("chars", "chars must be greater than 0");

            var result = _input.Substring(_position, chars);
            _position += chars;
            return result;
        }

        /// <summary>
        /// Consumes all of the remaining characters.
        /// </summary>
        /// <returns></returns>
        public string TakeAll() {
            var result = _input.Substring(_position);
            _position = _input.Length;
            return result;
        }

        /// <summary>
        /// Takes characters from the input while they match the given character set, and
        /// advances the parser. The parser will be positioned on the first character that
        /// doesn't match the supplied characters, or at end of input.
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        public string TakeWhile(params char[] chars) {
            if (chars == null)
                throw new ArgumentNullException("chars");

            var result = new StringBuilder();

            while (!AtEndOfInput) {
                var current = Current;
                if (chars.Contains(current)) {
                    result.Append(current);
                    _position++;
                } else {
                    return result.ToString();
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Takes characters from the input until the parser finds a character from the
        /// given character set, and advances the parser. The parser will be positioned on 
        /// the first character that doesn't match the character set, or at end of input.
        /// </summary>
        /// <param name="chars">The characters to match.</param>
        /// <param name="inclusive">
        /// If true, the delimiter will be included in the return value, and the parser will
        /// be positioned after the delimiter. If false, the delimiter won't be returned, 
        /// and the parser will be positioned on the delimiter.
        /// </param>
        /// <returns></returns>
        public string TakeUntil(bool inclusive, params char[] chars) {
            if (chars == null)
                throw new ArgumentNullException("chars");

            if (chars.Length == 0)
                throw new ArgumentException("chars must not be empty");

            var result = new StringBuilder();

            while (!AtEndOfInput) {
                var current = Current;
                if (chars.Contains(current)) {
                    if (inclusive) {
                        result.Append(current);
                        _position++;
                    }
                    return result.ToString();
                }

                result.Append(current);
                _position++;
            }

            return result.ToString();
        }
    }
}